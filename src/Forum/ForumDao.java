/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forum;

import Conexao.Conexao;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author Lucas
 */
public class ForumDao {
    public Forum save(Forum forum){
        SqlSession session = Conexao.getSqlSessionFactory().openSession();	
        ForumMapper mapper = session.getMapper(ForumMapper.class);

        if(forum.getId() == 0)
                mapper.insert(forum);
        else
    //			  mapper.update(forum);

        session.commit();
        session.close();
        return forum;
    }
}
